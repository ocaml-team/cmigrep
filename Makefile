SOURCES=misc.cmx tbl.cmx config.cmx clflags.cmx terminfo.cmx ccomp.cmx	\
warnings.cmx consistbl.cmx linenum.cmx location.cmx longident.cmx	\
syntaxerr.cmx parser.cmx lexer.cmx parse.cmx printast.cmx		\
unused_var.cmx ident.cmx path.cmx primitive.cmx types.cmx btype.cmx	\
oprint.cmx subst.cmx predef.cmx datarepr.cmx env.cmx typedtree.cmx	\
ctype.cmx printtyp.cmx includeclass.cmx mtype.cmx includecore.cmx	\
includemod.cmx parmatch.cmx typetexp.cmx stypes.cmx typecore.cmx	\
typedecl.cmx typeclass.cmx typemod.cmx cmigrep.ml

BYTESOURCES=misc.cmo tbl.cmo config.cmo clflags.cmo terminfo.cmo	\
ccomp.cmo warnings.cmo consistbl.cmo linenum.cmo location.cmo		\
longident.cmo syntaxerr.cmo parser.cmo lexer.cmo parse.cmo		\
printast.cmo unused_var.cmo ident.cmo path.cmo primitive.cmo types.cmo	\
btype.cmo oprint.cmo subst.cmo predef.cmo datarepr.cmo env.cmo		\
typedtree.cmo ctype.cmo printtyp.cmo includeclass.cmo mtype.cmo		\
includecore.cmo includemod.cmo parmatch.cmo typetexp.cmo stypes.cmo	\
typecore.cmo typedecl.cmo typeclass.cmo typemod.cmo cmigrep.ml


GODI_CONF=$(shell godi_confdir)
GODI_BASE=$(shell cat $(GODI_CONF)/godi.conf | grep ^LOCALBASE | sed -e 's/LOCALBASE *= *//')
GODI_LIB=$(GODI_BASE)/lib/ocaml/compiler-lib

all:
	ocamlfind ocamlopt -o cmigrep -I $(GODI_LIB) \
	-package pcre,findlib,unix -linkpkg $(SOURCES)

byte:
	ocamlfind ocamlc -o cmigrep -I $(GODI_LIB) \
	-package pcre,findlib,unix -linkpkg $(BYTESOURCES)

install:
	cp cmigrep $(GODI_BASE)/bin

clean:
	@rm -f *.o *.cmo *.cmi *.cma *.cmx *.cmi cmigrep
