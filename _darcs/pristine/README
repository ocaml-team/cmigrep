GODI Recomended

To build, type make.

If it doesn't work, you probably aren't using godi.  You'll have to
edit the make file to tell it where your compiler sources are, as well
as possibly rewrite the very simple build rule to not use findlib.

A short description of features,

cmigrep: <args> <module-expr> 

cmigrep has two modes, the first and most common is that of searching
for various types of objects inside a module. Objects that you can
search for include

switch         purpose
-t             (regexp) print types with matching names
-r             (regexp) print record field labels with matching names
-c             (regexp) print constructors with matching names
-p             (regexp) print polymorphic variants with matching names
-e             (regexp) print exceptions with matching constructors
-v             (regexp) print values with matching names
-o             (regexp) print all classes with matching names
-a             (regexp) print all names which match the given expression

These are all very useful for finding specific things inside a given
module. Here are a few examples,

find some constructors in the unix module

itsg106:~ eric$ cmigrep -c SO_ Unix 
SO_DEBUG (* socket_bool_option *)
SO_BROADCAST (* socket_bool_option *)
SO_REUSEADDR (* socket_bool_option *)
SO_KEEPALIVE (* socket_bool_option *)
SO_DONTROUTE (* socket_bool_option *)
SO_OOBINLINE (* socket_bool_option *)
SO_ACCEPTCONN (* socket_bool_option *)
SO_SNDBUF (* socket_int_option *)
SO_RCVBUF (* socket_int_option *)
SO_ERROR (* socket_int_option *)
SO_TYPE (* socket_int_option *)
SO_RCVLOWAT (* socket_int_option *)
SO_SNDLOWAT (* socket_int_option *)
SO_LINGER (* socket_optint_option *)
SO_RCVTIMEO (* socket_float_option *)
SO_SNDTIMEO (* socket_float_option *)

find the same constructors, but this time anywhere

itsg106:~/cmigrep-1.1 eric$ ./cmigrep -c SO_ \*
SO_DEBUG (* UnixLabels.socket_bool_option *)
SO_BROADCAST (* UnixLabels.socket_bool_option *)
SO_REUSEADDR (* UnixLabels.socket_bool_option *)
SO_KEEPALIVE (* UnixLabels.socket_bool_option *)
SO_DONTROUTE (* UnixLabels.socket_bool_option *)
SO_OOBINLINE (* UnixLabels.socket_bool_option *)
SO_ACCEPTCONN (* UnixLabels.socket_bool_option *)
SO_SNDBUF (* UnixLabels.socket_int_option *)
SO_RCVBUF (* UnixLabels.socket_int_option *)
SO_ERROR (* UnixLabels.socket_int_option *)
SO_TYPE (* UnixLabels.socket_int_option *)
SO_RCVLOWAT (* UnixLabels.socket_int_option *)
SO_SNDLOWAT (* UnixLabels.socket_int_option *)
SO_LINGER (* UnixLabels.socket_optint_option *)
SO_RCVTIMEO (* UnixLabels.socket_float_option *)
SO_SNDTIMEO (* UnixLabels.socket_float_option *)
SO_DEBUG (* Unix.socket_bool_option *)
SO_BROADCAST (* Unix.socket_bool_option *)
SO_REUSEADDR (* Unix.socket_bool_option *)
SO_KEEPALIVE (* Unix.socket_bool_option *)
SO_DONTROUTE (* Unix.socket_bool_option *)
SO_OOBINLINE (* Unix.socket_bool_option *)
SO_ACCEPTCONN (* Unix.socket_bool_option *)
SO_SNDBUF (* Unix.socket_int_option *)
SO_RCVBUF (* Unix.socket_int_option *)
SO_ERROR (* Unix.socket_int_option *)
SO_TYPE (* Unix.socket_int_option *)
SO_RCVLOWAT (* Unix.socket_int_option *)
SO_SNDLOWAT (* Unix.socket_int_option *)
SO_LINGER (* Unix.socket_optint_option *)
SO_RCVTIMEO (* Unix.socket_float_option *)
SO_SNDTIMEO (* Unix.socket_float_option *)

It seems they only exist in the unix module. By default cmigrep
searches the current directory, and the standard library. You can of
course tell it to search elsewhere.

full types get printed in the case that the constructors have
arguments. Notice that adding to the include path is modeled after the
compiler. Findlib is also supported.

itsg106:~ eric$ cmigrep -c "^Tsig_.*" -I /opt/godi/lib/ocaml/compiler-lib Types
Tsig_value of Ident.t * value_description (* signature_item *)
Tsig_type of Ident.t * type_declaration * rec_status (* signature_item *)
Tsig_exception of Ident.t * exception_declaration (* signature_item *)
Tsig_module of Ident.t * module_type * rec_status (* signature_item *)
Tsig_modtype of Ident.t * modtype_declaration (* signature_item *)
Tsig_class of Ident.t * class_declaration * rec_status (* signature_item *)
Tsig_cltype of Ident.t * cltype_declaration * rec_status (* signature_item *)

record field labels
itsg106:~ eric$ cmigrep -r "^st_" Unix
st_dev: int (* stats *)
st_ino: int (* stats *)
st_kind: file_kind (* stats *)
st_perm: file_perm (* stats *)
st_nlink: int (* stats *)
st_uid: int (* stats *)
st_gid: int (* stats *)
st_rdev: int (* stats *)
st_size: int (* stats *)
st_atime: float (* stats *)
st_mtime: float (* stats *)
st_ctime: float (* stats *)

findlib support, matching value names
itsg106:~ eric$ cmigrep -package pcre -v for Pcre
val foreach_line : ?ic:in_channel -> (string -> unit) -> unit 
val foreach_file : string list -> (string -> in_channel -> unit) -> unit 

nested modules
itsg106:~ eric$ cmigrep -v ".*" Unix.LargeFile
val lseek : file_descr -> int64 -> seek_command -> int64 
val truncate : string -> int64 -> unit 
val ftruncate : file_descr -> int64 -> unit 
val stat : string -> stats 
val lstat : string -> stats 
val fstat : file_descr -> stats 

types
itsg106:~ eric$ cmigrep -t ".*" Unix.LargeFile
type stats = {
  st_dev : int;
  st_ino : int;
  st_kind : file_kind;
  st_perm : file_perm;
  st_nlink : int;
  st_uid : int;
  st_gid : int;
  st_rdev : int;
  st_size : int64;
  st_atime : float;
  st_mtime : float;
  st_ctime : float;
}

everything!
itsg106:~ eric$ cmigrep -a ".*" Unix.LargeFile
val lseek : file_descr -> int64 -> seek_command -> int64
val truncate : string -> int64 -> unit
val ftruncate : file_descr -> int64 -> unit
type stats = {
  st_dev : int;
  st_ino : int;
  st_kind : file_kind;
  st_perm : file_perm;
  st_nlink : int;
  st_uid : int;
  st_gid : int;
  st_rdev : int;
  st_size : int64;
  st_atime : float;
  st_mtime : float;
  st_ctime : float;
}
val stat : string -> stats
val lstat : string -> stats
val fstat : file_descr -> stats

exception declarations
itsg106:~/cmigrep eric$ ./cmigrep -e ".*" Unix
exception Unix_error of error * string * string

toplevel modules starting with Net

itsg106:~/cmigrep-1.1 eric$ ./cmigrep -m -package netstring Net*
Neturl
Netulex
Netstring_top
Netstring_str
Netstring_pcre
Netstring_mt
Netstream
Netsendmail
Netmime
Netmappings_other
Netmappings_min
Netmappings_jp
Netmappings_iso
Netmappings
Nethttp
Nethtml_scanner
Nethtml
Netencoding
Netdb
Netdate
Netconversion
Netchannels
Netbuffer
Netaux
Netaddress
Netaccel_link
Netaccel

sub modules one level deep of modules starting with Net

itsg106:~/cmigrep-1.1 eric$ ./cmigrep -m -package netstring Net*.*
Netulex.ULB
Netulex.Ulexing
Nethttp.Header
Netencoding.Base64
Netencoding.QuotedPrintable
Netencoding.Q
Netencoding.Url
Netencoding.Html
Netaux.KMP
Netaux.ArrayAux

